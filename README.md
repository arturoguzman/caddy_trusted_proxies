#Caddy trusted proxies

Fetches trusted ips from Bunny CDN and writes them into a file
for Caddy to use as trusted proxies

Add your own trusted proxies (comma separated):

```
node index.js --proxies private_range
```

```
node index.js --proxies 192.168.0.1,10.1.1.0/24
```

Add a save location:

```
node index.js --proxies private_range --save ~/my_location
```
