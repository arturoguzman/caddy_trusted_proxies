import fs from 'node:fs'

const setup = process.argv.reduce((acc, arg, i) => {
	if (arg === '--proxies') {
		acc['proxies'] = process.argv[i + 1]
	}
	if (arg === '--save') {
		acc['save'] = process.argv[i + 1]
	}
	if (arg === '--token') {
		acc['token'] = process.argv[i + 1]
	}
	if (arg === '--url') {
		acc['url'] = process.argv[i + 1]
	}
	return acc
}, {})

async function main() {
	try {
		if (!setup.url) {
			console.log(`You need to specify a URL with --url`)
			process.exit()
		}
		const res = await fetch(setup.url)
		const { data } = await res.json()
		const ip = data[0].ip
		const split = setup.proxies?.replaceAll(',', ' ') ?? ''
		const result = `${ip} ${split}`
		fs.writeFileSync(setup.save ?? './trusted_ips.txt', result)
		console.log(`Trusted ips updated!`)
	} catch (error) {
		console.error(`There has been an error updating the trusted ips: ${error}`)
	}
}
main()
