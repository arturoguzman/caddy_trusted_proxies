import fs from 'node:fs'

function parseString(str) {
	return str.replaceAll(',', ' ').replaceAll('[', '').replaceAll(']', '').replaceAll(`"`, '')
}

const setup = process.argv.reduce((acc, arg, i) => {
	if (arg === '--proxies') {
		acc['proxies'] = process.argv[i + 1]
	}
	if (arg === '--save') {
		acc['save'] = process.argv[i + 1]
	}
	return acc
}, {})

async function main() {
	try {
		const res_ipv4 = await fetch('https://bunnycdn.com/api/system/edgeserverlist')
		const res_ipv6 = await fetch('https://bunnycdn.com/api/system/edgeserverlist/IPv6')
		const split = setup.proxies?.replaceAll(',', ' ') ?? ''
		const ipv4 = await res_ipv4.text()
		const ipv6 = await res_ipv6.text()
		const result = `trusted_proxies static ${split} ${parseString(ipv4)} ${parseString(ipv6)}`
		fs.writeFileSync(setup.save ?? './trusted_proxies.txt', result.replaceAll('  ', ' '))
		console.log(`Trusted proxies updated!`)
	} catch (error) {
		console.error(`There has been an error updating the trusted proxies: ${error}`)
	}
}
main()
